﻿using System;

namespace Enum
{
    class Program
    {
        enum Operation
        {
            Add,
            Sub,
            Mult,
            Div
        }
        static float MathOp(int x, int y, Operation operation){
            float result = 0;
            switch (operation)
            {
                case Operation.Add:
                    result = x + y;
                    break;
                case Operation.Sub:
                    result = x - y;
                    break;
                case Operation.Mult:
                    result = x * y;
                    break;
                case Operation.Div:
                    result = x / y;
                    break;
            }
            return result;
        }

        static void Main(string[] args)
        {
            int x = 0, y = 0;
            Operation operation = Operation.Add;//just default
            int numberOfOperation = 0;
            while (true)
            {
                Console.WriteLine("Enter number of operation:\n0 - Add\n1 - Sub\n2 - Mult\n3 - Div\n");
                numberOfOperation = int.Parse(Console.ReadLine());
                if (numberOfOperation < 0 || numberOfOperation > 3)
                {
                    Console.WriteLine("Input Error!");
                }
                else
                {
                    switch (numberOfOperation){
                        case 0:
                            operation = Operation.Add;
                            break;
                        case 1:
                            operation = Operation.Sub;
                            break;
                        case 2:
                            operation = Operation.Mult;
                            break;
                        case 3:
                            operation = Operation.Div;
                            break;
                    }
                    Console.WriteLine("Enter x, y:\n");
                    x = int.Parse(Console.ReadLine());
                    y = int.Parse(Console.ReadLine());
                    break;
                }
            }
            Console.WriteLine(MathOp(x, y, operation));
            Console.ReadLine();//not to close console
        }
    }
}
